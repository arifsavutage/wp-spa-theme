<?php 
/*
Template Name: Custom Home
*/
get_header();
?>
  
  
  <?php
    //manggil page dengan slug 'welcome'
    $cover = new WP_Query( array( 'pagename' => 'welcome' ) );

    if( $cover->have_posts() ) :

       $cover->the_post();

       //mengambil image url dari feature image
       $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
  ?>
  <div class="py-5 text-center" style="background-image: url('<?php echo $backgroundImg[0];?>');background-repeat:no-repeat;background-size:cover;background-position:center top;">
  <!--<div class="py-5 text-center bg-primary">-->
    <div class="container py-5">
      <div class="row">
        <div class="col-md-12">

          <?php
            the_content();
          ?>
          
          <a href="#kontak" class="btn btn-lg mx-1 btn-warning">Contact Us</a>
        </div>
      </div>
    </div>
  </div>
  <?php endif; ?>

  <div class="py-5">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          
          <?php
            if( is_active_sidebar( 'daily-quotes' ) ) :
              dynamic_sidebar ( 'daily-quotes' );
            endif;
          ?>

        </div>
      </div>
    </div>
  </div>
  <div class="py-5 text-white bg-secondary" id="paket">
    <div class="container">
      <div class="row">
        <div class="align-self-center p-5 col-md-6">

          <?php
            if( is_active_sidebar( 'about' ) ) :
              dynamic_sidebar ( 'about' );
            endif;
          ?>

          <!-- Warning -->
          <?php
            if( is_active_sidebar( 'warning' ) ) :
              dynamic_sidebar ( 'warning' );
            endif;
          ?>

        </div>
        <div class="col-md-6 p-0">          
          <div id="carousel1" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
              <?php
              $pktqry = new WP_Query( array( 'category_name' => 'paket' ) );
              if( $pktqry->have_posts() ) : 
              ?>
              <?php
                $x=1;
                while( $pktqry->have_posts() ) :

                  $pktqry->the_post();
              ?>
              <div class="carousel-item <?php if($x == 1) echo "active";?>">
                <?php the_post_thumbnail('thumbnail', array('class'=>'d-block img-fluid w-100'));?>
                <div class="carousel-caption">
                  <h3><?php the_title();?></h3>
                </div>
              </div>
              <?php
                    $x++;
                  endwhile;
                endif;
              ?>
            </div>
            <a class="carousel-control-prev" href="#carousel1" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel1" role="button" data-slide="next">
              <span class="carousel-control-next-icon"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <div class="py-5" id="artikel">
    <div class="container">
      <?php
      $query = new WP_Query( array( 'category_name' => 'artikel' ) );
      if( $query->have_posts() ) : 
      ?>
      <?php
        $x=1;
        while( $query->have_posts() ) :

          $query->the_post();

          if($x%2 == 0) :
      ?>
          <div class="row mb-5">
            <div class="col-md-7">
            <h2 class="text-primary"><?php the_title();?></h2>
            <?php the_content(sprintf( 
            __('Selengkapnya', 'befitsmg' ), 
            the_title('<a class="btn btn-lg btn-outline-primary">', '</a>', false) 
          ));?>
            
            </div>
            <div class="col-md-5 align-self-center">
              <?php the_post_thumbnail('thumbnail', array('class'=>'img-fluid d-block w-100 img-thumbnail'));?>
            </div>
          </div>
      <?php else : ?>
            <div class="row">
              <div class="col-md-5">
                <?php the_post_thumbnail('thumbnail', array('class'=>'img-fluid d-block mb-4 w-100 img-thumbnail'));?>
              </div>
              <div class="col-md-7">
                <h2 class="text-primary pt-3"><?php the_title();?></h2>
                <?php the_content(sprintf( 
            __('Selengkapnya', 'befitsmg' ), 
            the_title('<a class="btn btn-lg btn-outline-primary">', '</a>', false) 
          ));?>
              </div>
            </div>
      <?php endif;
          $x++;
        endwhile;
      ?>
      <?php endif;?>  
      
      <?php
        if( is_active_sidebar( 'blog-link' ) ) :
          dynamic_sidebar ( 'blog-link' );
        endif;
      ?>
    </div>
  </div>

<?php
get_footer();
?>