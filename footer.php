<div class="py-5 bg-dark text-white" id="kontak">
    <div class="container">
      <div class="row">
        <div class="col-md-6 text-center align-self-center">
          <p class="mb-5" id="kontak">
            <?php
              if( is_active_sidebar( 'footer-kontak' ) ) :
                dynamic_sidebar ( 'footer-kontak' );
              endif;
            ?>
          
          <div class="my-3 row">
            <div class="col-4">
              <a href="https://www.facebook.com" target="_blank">
                <i class="fa fa-3x fa-facebook"></i>
              </a>
            </div>
            <div class="col-4">
              <a href="https://twitter.com" target="_blank">
                <i class="fa fa-3x fa-twitter"></i>
              </a>
            </div>
            <div class="col-4">
              <a href="https://www.instagram.com" target="_blank">
                <i class="fa fa-3x fa-instagram"></i>
              </a>
            </div>
          </div>
        </div>
        <div class="col-md-6 p-0">
          <?php
              if( is_active_sidebar( 'footer-maps' ) ) :
                dynamic_sidebar ( 'footer-maps' );
              endif;
            ?>
        </div>
      </div>
    </div>
  </div>
  <?php wp_footer();?>
</body>

</html>