<div class="row" id="post-<?php the_ID();?>">
  <div class="col-md-5">
    <?php the_post_thumbnail('post-thumbnail', array('class' => ('img-fluid d-block mb-4 w-100 img-thumbnail'))); ?>
  </div>
  <div class="col-md-7">
    <h2 class="text-primary pt-3">
      <a href="<?php the_permalink(); ?>"> <?php the_title(); ?></a>
    </h2>
    <div class="entry-meta">
        <?php edit_post_link( __( 'Edit', 'befitsmg', '</span>' ) );?>        
    </div>

      <?php
        the_content( 
          sprintf( 
            __('Selengkapnya', 'befitsmg' ), 
            the_title('<a class="btn btn-lg btn-outline-primary">', '</a>', false) 
          ) 
        );?>
  </div>
</div>