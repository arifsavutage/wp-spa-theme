<?php get_header(); ?>
<div class="py-5" id="artikel">
    <div class="container">
<?php
	while( have_posts() ) :
		the_post();
?>
		<div class="row mb-5" id="post-<?php the_ID();?>">
			<div class="col-md-12 align-self-center">
				<?php the_post_thumbnail('post-thumbnail', array('class' => ('img-fluid d-block w-100 img-thumbnail'))); ?>
			</div>
		</div>
		<div class="row mb-5">

			<div class="col-md-12">
				<h2 class="text-primary">
					<a href="<?php the_permalink(); ?>"> <?php the_title(); ?></a>
				</h2>

				<div class="entry-meta">
				<?php edit_post_link( __( 'Edit', 'befitsmg', '</span>' ) );?>        
				</div>

				<?php
				the_content( 
				  sprintf( 
				    __('Selengkapnya', 'befitsmg' ), 
				    the_title('<a class="btn btn-lg btn-outline-primary">', '</a>', false) 
				  ) 
				);?>

			</div>
		</div>
<?php
	endwhile;
?>
	</div>
</div>
<?php get_footer(); ?>