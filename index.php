<?php
	get_header();
?>
<div class="py-5" id="artikel">
    <div class="container">
    	<?php if( have_posts() ) : ?>
    		
    		<!--
    		<?php if (is_home() && ! is_front_page() ) :?>
			<div class="row mb-5">
				<div class="col-md-7">
				  <h2 class="text-primary"><?php single_post_title();?></h2>
				  <?php the_content();?>
				</div>
				<div class="col-md-5 align-self-center">
					<?php the_post_thumbnail('post-thumbnail', array('class' => ('img-fluid d-block w-100 img-thumbnail')));?>
				  	
				</div>
			</div>
			<?php endif;?>
			-->

			<?php
			$x=1;
			while( have_posts() ) :

				the_post();

				if($x%2 == 0) :
					get_template_part('content', get_post_format());
				else :
					get_template_part('flip-content', get_post_format());
				endif;
				$x++;
			endwhile;
			?>
		<?php 
			else :
				get_template_part('content', 'none');
			endif;
			wp_reset_query();
		?>

		<ul class="pager">
          <li>
            <?php previous_posts_link( '←  New Post' );?>
          </li>
          <li>
            <?php next_posts_link( 'Older Post  →' );?>
          </li>
        </ul>

    </div>
</div>
<?php get_footer(); ?>