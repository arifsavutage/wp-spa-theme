<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Be fit adalah website layanan jasa terapis pijat ataupun spa online.">
  <meta name="keywords" content="jasa terapis semarang, spa online, jasa pijat spa, pijat semarang">
  <!--<link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css" type="text/css">
  <link rel="stylesheet" href="style.css" type="text/css">-->
  
  <link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  <!--
  <link rel="stylesheet" href="bootstrap4/css/bootstrap.min.css" type="text/css">
-->
<?php wp_head();?>
</head>

<body>
  <nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <div class="container">
      <a class="navbar-brand" href="#">
        <?php
          /*Menampilkan logo atau site title*/
          $custom_logo_id = get_theme_mod( 'custom_logo' );
          $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
          if ( has_custom_logo() ) {
                  echo '<img src="'. esc_url( $logo[0] ) .'">';
                  echo "<b class='brand-name'>" . get_bloginfo( 'name' ) . "</b>";
          } else {
                  echo "<b class='brand-name'>" . get_bloginfo( 'name' ) . "</b>";
          }
        ?>
      </a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar2SupportedContent">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse text-center justify-content-end" id="navbar2SupportedContent">
        
        <!--
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="#">Beranda</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#paket">Paket Spa</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#artikel">Artikel</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#kontak">Kontak</a>
          </li>
        </ul>
        -->

        <?php
          wp_nav_menu( array(
            'theme_location'  => 'primary',
            'menu_class'      => 'navbar-nav',
            'menu_id'         => 'primary-menu'
          ) );
        ?>

        <a class="btn navbar-btn ml-2 btn-success text-light" href="#" onclick="window.location.href='https://api.whatsapp.com/send?phone=6281234567890&text=Hallo%20Kak,%20mau%20tanya%20paket%20spa%20dong'">
          <i class="fa d-inline fa-lg fa-whatsapp"></i> Chat</a>
      </div>
    </div>
  </nav>