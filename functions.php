<?php
load_theme_textdomain( 'befitsmg' );
/*
membuat support featured image di theme
*/
add_theme_support( 'post-thumbnails' );

/*title tag*/
add_theme_support( 'title-tag' );

/*register menu navigasi*/
function register_befitsmg_menus(){
	register_nav_menus(
		array(
			'primary'		=> __( 'Top Menu' ),
			'social-menu'	=> __( 'Social Menu' ),
			'mobile-menu'	=> __( 'Mobile Menu' )
		)
	);
}
add_action('init', 'register_befitsmg_menus');

/*
membuat custom logo di customizing
*/
function themename_custom_logo_setup() {
    $defaults = array(
        'height'      => 100,
        'width'       => 400,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array( 'site-title', 'site-description' ),
    );
    add_theme_support( 'custom-logo', $defaults );
}
add_action( 'after_setup_theme', 'themename_custom_logo_setup' );

/*
membuat widget
*/
function befitsmg_widgets_init(){
	register_sidebar(array(
		'name'			=> __('Daily Quotes', 'befitsmg'),
		'id'			=> 'daily-quotes',
		'description'	=> __('Menampilkan daily quotes', 'befitsmg'),
		'before_widget'	=> '',
		'after_widget'	=> '',
		'before_title'	=> '<h1 class="text-center display-3 text-primary">',
		'after_title'	=> '</h1>'
	));
	
	register_sidebar(array(
		'name'			=> __('Befit About', 'befitsmg'),
		'id'			=> 'about',
		'description'	=> __('Menampilkan deskripsi tentang About Us', 'befitsmg'),
		'before_widget'	=> '',
		'after_widget'	=> '',
		'before_title'	=> '<h1 class="mb-4">',
		'after_title'	=> '</h1>'
	));
	
	register_sidebar(array(
		'name'			=> __('Befit Warning', 'befitsmg'),
		'id'			=> 'warning',
		'description'	=> __('Menampilkan deskripsi warning', 'befitsmg'),
		'before_widget'	=> '',
		'after_widget'	=> '',
		'before_title'	=> '<p class="mb-5" style="background-color: red;padding:15px;font-size: 28px;color: #ffffff;">',
		'after_title'	=> '</p>'
	));

	
	register_sidebar(array(
		'name'			=> __('Blog Link', 'befitsmg'),
		'id'			=> 'blog-link',
		'description'	=> __('Menampilkan link blog list', 'befitsmg'),
		'before_widget'	=> '',
		'after_widget'	=> '',
		'before_title'	=> '',
		'after_title'	=> ''
	));
	
	register_sidebar(array(
		'name'			=> __('Footer Kontak', 'befitsmg'),
		'id'			=> 'footer-kontak',
		'description'	=> __('Menampilkan kontak CS', 'befitsmg'),
		'before_widget'	=> '',
		'after_widget'	=> '',
		'before_title'	=> '<strong>',
		'after_title'	=> '</strong>'
	));

	register_sidebar(array(
		'name'			=> __('Footer Maps', 'befitsmg'),
		'id'			=> 'footer-maps',
		'description'	=> __('Menampilkan maps', 'befitsmg'),
		'before_widget'	=> '',
		'after_widget'	=> '',
		'before_title'	=> '',
		'after_title'	=> ''
	));
}
add_action( 'widgets_init' , 'befitsmg_widgets_init' );

function wpbefitsmg_style(){
	wp_enqueue_style('fontsawesome', get_template_directory_uri().'/font-awesome/css/font-awesome.min.css', array(), '4.7', 'all');
}
add_action('wp_enqueue_scripts', 'wpbefitsmg_style');

function wpbefitsmg_scripts(){

	wp_enqueue_script( 'jquerys', get_template_directory_uri() . '/bootstrap4/js/jquery-3.2.1.slim.min.js', array('jquery'),'3', true);
	wp_enqueue_script( 'poppers', get_template_directory_uri() . '/bootstrap4/js/popper.min.js', array(), '1.1', true);
	wp_enqueue_script( 'bootstraps', get_template_directory_uri() . '/bootstrap4/js/bootstrap.min.js', array(), '4.1', true);
}
add_action('wp_footer', 'wpbefitsmg_scripts');

?>